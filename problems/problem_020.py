# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    number_attendee = len(attendees_list)
    number_member = len(members_list)
    return number_attendee / number_member >= 0.5
