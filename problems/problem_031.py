# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_squares(values):
    if len(values) <= 1:
        return None
    result = 0
    for item in values:
        result += item * item
        return result

def sum_of_squares(values):
    if len(values) <= 1:                                # solution
        return None                                     # solution
    result = 0                                          # solution
    for value in values:                                # solution
        result += value * value                         # solution
    return result

# Note I don't know why when I ran test above the result for the one i wrote
# = 1 but when I ran the test using the solution the result = 14

values = [1, 2, 3]
print(sum_of_squares(values))
