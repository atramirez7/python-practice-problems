# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    list_of_numbers = values
    if len(list_of_numbers) == 0:
        return None
    else:
        return max(list_of_numbers)

list_of_numbers = [3, 13, 9, 8, 2]
print(max_in_list(list_of_numbers))
