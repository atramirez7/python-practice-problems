# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    list_of_stuff = []
    if is_workday and not is_sunny:
        list_of_stuff.append("umbrella")
    if is_workday:
        list_of_stuff.append("laptop")
    else:
        list_of_stuff.append("surfboard")
    return list_of_stuff

is_workday = False
is_sunny = False
print(gear_for_day(is_workday, is_sunny))
