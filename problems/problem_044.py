# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.


# for each item in the key_list match any key in the dictionary then
# return the value of that key in the list


def translate(key_list, dictionary):
    new_list = []
    for key in key_list:
        if key in dictionary:
            new_list.append(dictionary.get(key))
        else:
            new_list.append(None)
    return new_list


# def translate(key_list, dictionary):
#     result = []

#     for i in range(len(key_list)):
#         if key_list[i] in dictionary:
#             result += dictionary.values()
#             return result
#         else:
#             return "Not Found"


keys = ["name", "age"]
dictionary = {"name": "Nick", "age": 18}
print(translate(keys, dictionary))
