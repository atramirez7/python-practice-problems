# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    list_of_number = values
    remove_dups = list(set(list_of_number))
    remove_dups.sort()
    if len(remove_dups) == 0:
        return None
    elif len(remove_dups) == 1:
        return remove_dups[0]
    else:
        return remove_dups[-2]

values = [1, 8, 8, 5, 62, 4, 12, 12, 62]
print(find_second_largest(values))
