# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# n = 0 <= 0
# 0 = 0
# add 1 count from 0 to 1 and then sum of both number
# add 2 count from 0 to 2 and then sum of all numbers

def sum_of_first_n_numbers(limit):
    if limit < 0:
        return None
    sum = 0
    for n in range(limit+1):
        sum = sum + n
    return sum
        # sum = sum + n



print(sum_of_first_n_numbers(5))
