# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.

#New Function = zip function -> Iterate over several iterables in parallel
#producing tuples with an item from each one

def pairwise_add(list1, list2):
    result_new_list = []
    for item in zip(list1, list2):
        value = sum(item)
        result_new_list.append(value)
    return result_new_list


list1 = [1, 2, 3, 4]
list2 = [4, 5, 6, 7]

print(pairwise_add(list1, list2))
