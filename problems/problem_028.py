# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
# use set()

# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    list_of_letter = list(s)
    remove_dups = set(list_of_letter)
    return "". join(remove_dups)

s = "abcaabc"
print(remove_duplicate_letters(s))

# when I use set() and run test, each time it prints the letter randomly
