# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

# create a new dictionary that
# input key turn into value and input value turn into key

def reverse_dictionary(dictionary):
    new_dict = dict()
    for item in dictionary.items():
        key = item[1]
        value = item[0]
        new_dict[key] = value
    return new_dict


dictionary = {"one": 1, "two": 2, "three": 3}
print(reverse_dictionary(dictionary))
