# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    average_scores = sum(values) / len(values)
    if average_scores >= 90:
        return "A"
    elif average_scores >= 80 and average_scores < 90:
        return "B"
    elif average_scores >= 70 and average_scores < 80:
        return "C"
    elif average_scores >= 60 and average_scores < 70:
        return "D"
    else:
        return "F"

list_of_scores = [90, 92, 76, 80]

print(calculate_grade(list_of_scores))
